import React, {Component, Fragment} from 'react';
import Input from './components/Input/Input';
import Item from './components/Item/Item';

import './App.css';

class App extends Component {
    state = {
        items: [
            {name: 'Buy milk', id: 0, done: false},
            {name: 'Play games', id: 1, done: false},
            {name: 'Do homework', id: 2, done: false}
        ],
        text: ''
    };

    changeHandler = event => {
        this.setState({text: event.target.value})
    };

    createNewItem = (event, value) => {
        event.preventDefault();
        const items = [...this.state.items];
        const newItem = {name: value, id: items.length, done: false};
        items.push(newItem);

        this.setState({items: items, text: ''});
    };

    deleteElement = (event, id) => {
        event.preventDefault();
        const items = [...this.state.items];
        for (let key in items) {
            if (items[key].id === id) {
                items.splice(key, 1);
            }
        }

        this.setState({items});
    };

    makeTaskDone = (event, id) => {
        let items = [...this.state.items];
        for (let i = 0; i < items.length; i++) {
            if (items[i].id === id) {
                items[i].done = event.target.checked;
            }
        }

        this.setState({items});
    };


    render() {
        const items = this.state.items.map((item) => (
            <Item
                onChange={(event) => this.makeTaskDone(event, item.id)}
                nameOfClass={item.done ? 'uk-card uk-card-primary uk-card-body' : 'uk-card uk-card-default uk-card-body'}
                onClick={(event) => this.deleteElement(event, item.id)}
                key={item.id}
                name={item.name}
            />
        ));

        return (
            <Fragment>
                <Input
                    value={this.state.text}
                    onChange={event => this.changeHandler(event)}
                    onClick={(event) => this.createNewItem(event, this.state.text)}
                />
                <div className="container">
                    <div className="uk-grid uk-child-width-1-3">
                        {items}
                    </div>
                </div>
            </Fragment>
        );
    }
}

export default App;
